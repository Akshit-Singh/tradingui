import { AfterContentInit, Component, Input, OnInit } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';
import { Transaction } from '../shared/transaction';

@Component({
  selector: 'app-list-orders',
  templateUrl: './list-orders.component.html',
  styleUrls: ['./list-orders.component.css']
})
export class ListOrdersComponent implements OnInit, AfterContentInit {
  @Input() transactions: any;

  constructor(public restApi: RestApiService) {}

  //TODO: fix CORS error
  ngOnInit(): void {
    this.transactions()
  }

  ngAfterContentInit(): void {
    console.log(this.transactions);
  }
}
