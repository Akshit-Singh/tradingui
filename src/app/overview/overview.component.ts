import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseComponent } from '../base/base.component';
import { Transaction } from '../shared/transaction';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent extends BaseComponent implements OnInit {
  @Input() transactions: Transaction[] = [];

  constructor(public router: Router) {
    super(router);
   }

  ngOnInit(): void {
    console.log(this.transactions);
  }

}
