import { Component, OnInit, ViewChild } from '@angular/core';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { RestApiService } from './shared/rest-api.service';
import { Transaction } from './shared/transaction';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'tradingUI';
  transactions: Transaction[] =[];
  @ViewChild (PieChartComponent) child: PieChartComponent | undefined;

  constructor(public restApi: RestApiService) {}

  ngOnInit() {
    this.loadtransactions();
    console.log(this.transactions);
  }

  loadtransactions() {
    return this.restApi.getTransactions().subscribe(
      (data: Transaction[]) => {
        this.transactions = data;
        console.log(data[0])
      },
      err => {
        console.log("err");
      },
      () => {
        console.log("load finished" + this.transactions);
        console.log(this.child?.transactions);
        this.child?.loadtransactions();

      })
  }
}
