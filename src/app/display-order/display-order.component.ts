import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseComponent } from '../base/base.component';

@Component({
  selector: 'app-display-order',
  templateUrl: './display-order.component.html',
  styleUrls: ['./display-order.component.css']
})
export class DisplayOrderComponent extends BaseComponent implements OnInit {

  constructor(public router: Router) {
    super(router);
   }

  ngOnInit(): void {
  }

}
