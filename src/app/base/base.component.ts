import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-base',
  template: `
    <p>
      base works!
    </p>
  `,
  styles: [
  ]
})
export class BaseComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit(): void {
  }

  openPage(routename: string) {
    this.router.navigateByUrl(`/${routename}`);
  }

}
