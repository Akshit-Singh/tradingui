export class Transaction {
    constructor() {
        this.id = 0;
        this.stockTicker = "";
        this.price = 0;
        this.volume = 0;
        this.buyOrSell = "";
        this.statusCode = 0
        this.timeStamp = 0;
        this.cancelled = 0;
    }
    id: number;
    stockTicker: string;
    price: number;
    volume: number;
    buyOrSell: string;
    statusCode: number;
    timeStamp: number;
    cancelled: number;
}
