import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { RestApiService } from '../shared/rest-api.service';
import { Transaction } from '../shared/transaction';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent {
  constructor(public restApi: RestApiService){}
  @Input() transactions: Transaction[] = [];

  public pieChartOptions: ChartOptions = {
    responsive: true}

  public pieChartLabels: Label[] = [];
  public pieChartData: SingleDataSet = [];

  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColor: any[] = [
    {
      backgroundColor:["#FF7360", "#6FC8CE", "#FAFFF2", "#FFFCC4", "#B9E8E0"]
    }];

  onChartClick(event: any){
    console.log(event);
  }

  drawChart(data: any) {
    // return this.restApi.getTransactions().subscribe((data: Transaction[]) => {
        // let data = this.transactions;
        console.log(data);
        console.log("Draw chart!!")
        let tempPositions = new Map()

        for (let i = 0; i < data.length; i++) {
          let ticker = data[i].stockTicker;
          let position = data[i].price * data[i].volume;

          if (data[i].cancelled != 1) {
            // add if order is buy
            if (data[i].buyOrSell == "buy") {
              if (tempPositions.has(ticker)) {
                let currentPosition = tempPositions.get(ticker);
                currentPosition += position;
                tempPositions.set(ticker, currentPosition);
              }
              else {
                tempPositions.set(ticker, position);
              }

            }
            // subtract if order is sell
            else if (data[i].buyOrSell == "sell") {
              if (tempPositions.has(ticker)) {
                let currentPosition = tempPositions.get(ticker);
                currentPosition -= position;
                tempPositions.set(ticker, currentPosition);
              }
            }

            // cannot have a negative position, so removing from map
            if (tempPositions.get(ticker) <= 0){
              tempPositions.delete(ticker)
            }
          }
        }

        let keys = Array.from(tempPositions.keys());
        let values = Array.from(tempPositions.values());

        // getting keys into the correct format
        let cleanedKeys = [];
        for (let i = 0; i < keys.length; i++) {
          cleanedKeys.push([keys[i]]);
        }

        this.pieChartLabels = cleanedKeys;
        this.pieChartData = values;

        console.log(cleanedKeys);
        console.log(values);

        // this.transactions = data;
  }

  loadtransactions() {
    return this.restApi.getTransactions().subscribe(
      (data: Transaction[]) => {
        // this.transactions = data;
        this.drawChart(data);
        console.log(data[0])
      },
      err => {
        console.log("err");
      })
  }

  ngOnInit(): void {
    this.loadtransactions()
    console.log(this.pieChartLabels);
    console.log(this.pieChartData);
    // this.pieChartData = [1.2,2.2,3.2,4.2,5.2];
  }

}
