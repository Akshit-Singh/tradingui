import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BaseComponent } from './base/base.component';
import { OverviewComponent } from './overview/overview.component';
import { DisplayOrderComponent } from './display-order/display-order.component';
import { ListOrdersComponent } from './list-orders/list-orders.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { PnlComponent } from './pnl/pnl.component';
import { LineChartComponent } from './line-chart/line-chart.component';



@NgModule({
  declarations: [
    AppComponent,
    BaseComponent,
    OverviewComponent,
    DisplayOrderComponent,
    ListOrdersComponent,
    LineChartComponent,
    PieChartComponent,
    PnlComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
