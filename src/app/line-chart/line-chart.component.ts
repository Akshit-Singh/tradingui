import { Component, OnInit } from '@angular/core';

import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { RestApiService } from '../shared/rest-api.service';
import { Transaction } from '../shared/transaction';
@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit{
  transactions: Transaction[] = [];
  

  constructor(public restApi: RestApiService) {}
  ngOnInit(): void {
    this.loadtransactions();
  }

  loadtransactions() {
    return this.restApi.getTransactions().subscribe((data: Transaction[]) => { 

      var ticker : string;
      this.transactions = data;
      
     
      ticker  = data[0].stockTicker;
      console.log(typeof ticker); 
      
    })
    
  }
  
       
  public lineChartData: ChartDataSets[] = [
    { data: [144.84, 146.55, 148.76, 149.26, 148.48], label:'APPL' },
  ]; 
    
  lineChartLabels: Label[] = ['15th Oct', '18th Oct', '19th Oct', '20th Oct', '21st Oct'];

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,255,0,0.28)',
    },
  ];

  lineChartLegend = false;
  lineChartPlugins = [];
  lineChartType: ChartType = 'line';

}






