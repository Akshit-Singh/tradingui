import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DisplayOrderComponent } from './display-order/display-order.component';
import { ListOrdersComponent } from './list-orders/list-orders.component';
import { OverviewComponent } from './overview/overview.component';

const routes: Routes = [
  { path: '', component: ListOrdersComponent },
  { path: 'listorders', component: ListOrdersComponent },
  { path: 'displayorder', component: DisplayOrderComponent },
  { path: 'overview', component: OverviewComponent },
  { path: '**', component: ListOrdersComponent }
]
  
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
