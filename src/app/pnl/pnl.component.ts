import { Component, Input, OnInit } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';
import { Transaction } from '../shared/transaction';

@Component({
  selector: 'app-pnl',
  templateUrl: './pnl.component.html',
  styleUrls: ['./pnl.component.css']
})
export class PnlComponent implements OnInit {
  public totalPosition = 0;
  constructor(public restApi: RestApiService) { }
  @Input() transactions: Transaction[] = [];

  calculatePnl(data: any) {
    for (let i = 0; i < data.length; i++) {
      let ticker = data[i].stockTicker;
      let position = data[i].price * data[i].volume;

      if (data[i].cancelled != 1) {
        // add if order is buy
        if (data[i].buyOrSell == "buy") {
          this.totalPosition += position;
        }
        // subtract if order is sell
        else if (data[i].buyOrSell == "sell") {
          this.totalPosition -= position;
        }
      }
    }
    this.totalPosition = Math.round(this.totalPosition);
  }

  loadtransactions() {
    return this.restApi.getTransactions().subscribe(
      (data: Transaction[]) => {
        this.calculatePnl(data);
        // console.log(data[0])
      },
      err => {
        console.log("err");
      })
  }

  ngOnInit(): void {
    this.loadtransactions()
  }
}
